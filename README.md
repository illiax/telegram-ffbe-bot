# telegram-ffbe-bot

## Modulo importación de datos

Los datos son obtenidos de https://github.com/aEnigmatic/ffbe . 

## Variables de entorno en env/

crear archivo envvar.sh 

        #!/bin/bash
        export TOKEN="token"
        export DB_NAME="db"
        export DB_URL="mongodb://----/"

finalmente hacer (para agregarlas a la sesión)
    
    source env.sh 

#### Para docker-compose, hacer un archivo similar sin export ni comillas llamado app.env

## docker-compose

    sudo docker-compose build app

para buildear la imagen de node

    docker-compose up # -d

para levantar la db y el app (-d para dejar como daemon)

	sudo docker-compose up --no-deps -d app

para levantar solo el -d app

## update de db con nuevos datos

    docker exec -it telegramexviusbot_app_1 sh

una vez dentro correr el comando del container

    python import_data_to_mongo/update_db.py
